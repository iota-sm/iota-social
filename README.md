# iota-social
Social media framework using the IOTA Tangle

## Installation
1. `npm config set @iota-sm:registry https://gitlab.com/api/v4/packages/npm/`
2. `npm i @iota-sm/iota-social`
