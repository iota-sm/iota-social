'use strict';

const Kerl = require('@iota/kerl').default;
const converter = require('@iota/converter');
const consts = require('./consts');

let kerl = new Kerl();
kerl.initialize();

const POW_SUBSTRING = consts.get('POW_SUFFIX');

const powHash = payload => {
    let nonceInt = 0;
    let hash;

    while (payload.length % 81 !== 0) {
        payload += '9';
    }

    for (let attempts = 0; attempts < 100000; ++attempts) {
        let nonce = converter.trytes(converter.fromValue(nonceInt)).padEnd(81, '9');
        let nonceTrits = converter.trits(nonce);
        let payloadTrits = converter.trits(payload);

        kerl.absorb(nonceTrits, 0, nonceTrits.length);
        kerl.absorb(payloadTrits, 0, payloadTrits.length);

        let hashTrits = [];
        kerl.squeeze(hashTrits, 0, Kerl.HASH_LENGTH);
        let hashTemp = converter.trytes(hashTrits);

        if (hashTemp.substr(hashTemp.length - POW_SUBSTRING.length) === POW_SUBSTRING) {
            hash = hashTemp;
            break;
        } else {
            ++nonceInt;
        }
    }

    return hash;
};

module.exports = {
    powHash: powHash
};
