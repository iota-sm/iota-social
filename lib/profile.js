'use strict';

const core = require('@iota/core');
const converter = require('@iota/converter');
const extractJson = require('@iota/extract-json');
const crypto = require('./cryptography');
const hash = require('./hash');
const consts = require('./consts');

let storage;

const iota = core.composeAPI({
    provider: consts.get('NODE')
});

const concatTypedArrays = (a, b) => { // a, b TypedArray of same type
    var c = new (a.constructor)(a.length + b.length);
    c.set(a, 0);
    c.set(b, a.length);
    return c;
};

const padTrits = trits => {
    const originalLength = trits.length;
    let requiredLength;
    if (originalLength > 0) {
        requiredLength = Math.ceil(originalLength / 3.0) * 3;
    } else if (originalLength < 0) {
        requiredLength = Math.floor(originalLength / 3.0) * 3;
    } else {
        requiredLength = 3;
    }

    const padding = new Uint8Array(requiredLength - originalLength);
    return concatTypedArrays(trits, padding);
};

const toValidUsername = username => {
    if (/^[a-zA-Z]{1,17}$/.test(username)) {
        return username.toUpperCase();
    } else {
        // TODO handle this better
        console.error('Invalid username');
        process.exit(1);
    }
};

const usernameToTag = usernameAsTrytes => {
    if (/^[A-Z]{1,17}$/.test(usernameAsTrytes)) {
        return consts.get('APP_NAME').padEnd(consts.get('APP_NAME_LENGTH'), '9') + usernameAsTrytes;
    } else {
        // TODO handle this better
        console.error('Invalid username');
        process.exit(1);
    }
};

const send = async (seed, address, signature, tag, aliasPayload, toSend) => {
    await iota.getNodeInfo().then(async node => {
        if (Math.abs(node['latestMilestoneIndex'] - node['latestSolidSubtangleMilestoneIndex']) > 3) {
            console.log('Node is probably not synced!');
        } else {
            let message = {
                'address': address,
                'value': 0,
                'message': converter.asciiToTrytes(
                    `{"app": "${consts.get('APP_NAME')}", "protocol": "${consts.get('PROTOCOL_NAME')}",  "timestamp": "${(new Date()).toLocaleString()}", "aliasPayload": "${aliasPayload}", "payload": ${toSend}, "signature": "${signature}"}`
                ),
                'tag': tag
            };

            // TODO do PoW locally:
            // - <https://medium.com/bytes-io/iota-proof-of-work-remote-vs-local-explained-1cbd89392a79>
            // - <https://github.com/iotaledger/ccurl.interface.js>
            // let bundle = await iota.prepareTransfers(seed, [message]);

            // iota.getTransactionsToApprove(3).then(transactionsToApprove => {
            //     let pow = ccurl(
            //         transactionsToApprove.trunkTransaction,
            //         transactionsToApprove.branchTransaction,
            //         bundle,
            //         14
            //     );
            //     pow.start();
            //     return pow;
            // });


            await iota.prepareTransfers(seed, [message]).then(async trytes => {
                await iota.sendTrytes(trytes, 3, 14);
            }).catch(error => {
                console.log(error);
            });
        }
    }).catch(error => {
        console.log(`Request error: ${error.message}`);
    });
};

const uploadProfile = async (username, info, init = false) => {
    const usernameTrytes = toValidUsername(username);
    const tag = usernameToTag(usernameTrytes);
    const primaryChannelRoot = storage.has('channels.primary.root').value() ? storage.get('channels.primary.root').value() : '';

    const profile = {
        username: usernameTrytes,
        hash: '',
        channels: {
            primary: {
                root: primaryChannelRoot
            }
        },
        info: info
    };

    const pubKey = storage.get('rsa.publicKey').value();
    const pubKeyBase64 = crypto.base64Encode(pubKey);
    const pubKeyTrytes = converter.asciiToTrytes(pubKeyBase64);
    const pubKeyTrytesLen = converter.trytes(padTrits(converter.fromValue(pubKeyTrytes.length))).padEnd(4, '9');

    const encryptedTag = crypto.rsaEncrypt(tag.padEnd(27, '9'), pubKey);
    const encryptedTagBase64 = Buffer.from(encryptedTag).toString('base64');
    const encryptedTagTrytes = converter.asciiToTrytes(encryptedTagBase64);
    const encryptedTagTrytesLen = converter.trytes(padTrits(converter.fromValue(encryptedTagTrytes.length))).padEnd(4, '9');

    const aliasPayload = new String(pubKeyTrytesLen + pubKeyTrytes + encryptedTagTrytesLen + encryptedTagTrytes + '9999');

    const profileHash = init ? hash.powHash(aliasPayload) : storage.get('profile.hash').value();
    profile.hash = profileHash;

    const profileString = JSON.stringify(profile);
    const signature = crypto.rsaSign(profileString, storage.get('rsa.privateKey').value());
    storage.set('profile', profile).write();

    await send(storage.get('channels.primary.state.seed').value(), profileHash, signature, tag, aliasPayload, profileString);
};

const profileExists = () => {
    return storage.has('profile').value();
};

const verifyProfile = (profileMsg, profileHash/*, displayedTag*/) => {
    let isValid = false;

    const app = profileMsg['app'];
    const protocol = profileMsg['protocol'];
    const aliasPayload = profileMsg['aliasPayload'];
    const signature = profileMsg['signature'];
    const profile = profileMsg['payload'];

    if (app === consts.get('APP_NAME') && protocol === consts.get('PROTOCOL_NAME') && aliasPayload && signature) {
        const pubKeyTrytesLen = converter.value(converter.trits(aliasPayload.slice(0, 4)));
        const pubKey = crypto.base64Decode(converter.trytesToAscii(aliasPayload.slice(4, pubKeyTrytesLen)));
        // const encryptedTagTrytesLen = converter.value(converter.trits(aliasPayload.slice(4 + pubKeyTrytesLen, 8 + pubKeyTrytesLen)));
        // const tag = crypto.rsaDecrypt(crypto.base64Decode(converter.trytesToAscii(aliasPayload.slice(8 + pubKeyTrytesLen, 8 + pubKeyTrytesLen + encryptedTagTrytesLen))));

        isValid = (
            crypto.rsaVerify(JSON.stringify(profile), signature, pubKey)
            // && tag === displayedTag
            && profileHash.substr(profileHash.length - consts.get('POW_SUFFIX').length) === consts.get('POW_SUFFIX')
        );

    }

    return isValid;
};

const find = async username => {
    return new Promise((resolve, reject) => {
        let results = [];
        let tag = usernameToTag(toValidUsername(username));

        iota.findTransactionObjects({ tags: [tag] }).then(transactions => {
            const tails = [];
            transactions.forEach(transaction => {
                // TODO add more validation
                if (transaction['currentIndex'] === 0) {
                    tails.push(transaction['hash']);
                }
            });
            let bundleCount = tails.length;
            if (bundleCount === 0) {
                reject('No profiles found.');
            }
            tails.forEach(tail => {
                iota.getBundle(tail).then(bundle => {
                    const msg = JSON.parse(extractJson.extractJson(bundle));
                    const aliasHash = bundle[0]['address'];
                    const displayedTag = bundle[0]['tag'];

                    if (verifyProfile(msg, aliasHash, displayedTag)) {
                        let profile = msg['payload'];
                        profile['alias'] = displayedTag.slice(10).replace(/9/g, '') + aliasHash;
                        results.push(profile);
                    } else {
                        console.warn('Found invalid or fraud profile');
                    }
                    if (--bundleCount == 0) {
                        resolve(results);
                    }
                }).catch(err => {
                    reject(err);
                });
            });
        }).catch(error => {
            reject(error);
        });
    });
};

const me = () => {
    return storage.get('profile').value();
};

const isMe = alias => {
    return storage.get('profile.hash').value() === alias.slice(-81);
};

const get = alias => {
    return isMe(alias) ? me() : storage.get('following').find({ username: alias.slice(0, -81), hash: alias.slice(-81) }).value();
};

const following = () => {
    const toReturn = [];
    storage.get('following').value().forEach(user => {
        toReturn.push(user.username + user.hash);
    });
    return toReturn;
};

const followingDetails = () => {
    const toReturn = [];
    storage.get('following').value().forEach((user, index) => {
        toReturn.push(user);
        delete toReturn[index].publicKey;
    });
    return toReturn;
};

const unfollow = alias => {
    storage.get('following').remove({ hash: alias.slice(-81) }).write();
};

const follow = alias => {
    return new Promise((resolve, reject) => {
        const current = storage.get('following').value();
        current.forEach(profile => {
            if (profile.alias === alias) {
                reject('Already following this profile');
            }
        });

        const [ tag, address ] = [ usernameToTag(toValidUsername(alias.slice(0, -81))), alias.slice(-81) ];

        iota.findTransactionObjects({ addresses: [address], tags: [tag] }).then(transactions => {

            const tails = [];
            transactions.forEach(transaction => {
                if (transaction['currentIndex'] === 0) {
                    tails.push(transaction['hash']);
                }
            });
            let bundleCount = tails.length;
            // TODO allow profile modifications
            if (bundleCount > 1) {
                reject('More than one profile found.');
            } else if (bundleCount === 0) {
                reject('No profiles found.');
            }
            tails.forEach(tail => {
                iota.getBundle(tail).then(bundle => {
                    const msg = JSON.parse(extractJson.extractJson(bundle));

                    const app = msg['app'];
                    const protocol = msg['protocol'];
                    const aliasPayload = msg['aliasPayload'];
                    const signature = msg['signature'];
                    const profile = msg['payload'];

                    const pubKeyTrytesLen = converter.value(converter.trits(aliasPayload.slice(0, 4)));
                    const pubKey = crypto.base64Decode(converter.trytesToAscii(aliasPayload.slice(4, pubKeyTrytesLen)));

                    if (app === consts.get('APP_NAME') && protocol === consts.get('PROTOCOL_NAME') && aliasPayload && signature) {
                        profile.publicKey = pubKey;
                        storage.get('following').push(profile).write();
                        resolve();
                    }

                }).catch(err => {
                    reject(err);
                });
            });
        }).catch(error => {
            reject(error);
        });
    });
};

module.exports = storageProvider => {
    storage = storageProvider;

    return {
        uploadProfile: uploadProfile,
        profileExists: profileExists,
        find: find,
        me: me,
        isMe: isMe,
        get: get,
        following: following,
        followingDetails: followingDetails,
        unfollow: unfollow,
        follow: follow
    };
};
