'use strict';

// eslint-disable-next-line no-undef
const isBrowser = typeof window !== 'undefined' && typeof window.document !== 'undefined';
const isNode = typeof process !== 'undefined' && process.versions != null && process.versions.node != null;

let mam;
if (isNode) {
    // console.log('Running MAM in node');
    mam = require('@iota/mam');
} else if (isBrowser) {
    // console.log('Running MAM in web');
    mam = require('@iota/mam/lib/mam.web.min.js');
}
const converter = require('@iota/converter');
const consts = require('./consts');

const MAM_MODE = 'public';

let initialised = false;
let storage;
let mamState;

// Initialise IOTA MAM State
const init = async (initMessage = 'Start of Iota Social feed') => {
    if (storage.has('channels.primary.state').value()) {
        mamState = mam.init(consts.get('NODE'), storage.get('channels.primary.state.seed').value());
        mamState['channel'] = storage.get('channels.primary.state.channel').cloneDeep().value();
        storage.get('following').value().forEach(profile => {
            mam.subscribe(mamState, profile.channels.primary.root, profile.channels.primary.details.mode);
        });
        initialised = true;
    } else {
        mamState = mam.init(consts.get('NODE'));
        storage.set('channels.primary.state', mamState).write();
        if (storage.has('channels.primary.root').value()) {
            storage.unset('channels.primary.root').write();
        }
        initialised = true;
        await send(initMessage);
    }
};

const checkInitialised = () => {
    if (!initialised) {
        console.log('MAM must be initialised.');
        process.exit(1);
    }
};

// Save MAM State
const saveMAM = newState => {
    mamState = newState;
    storage.set('channels.primary.state', mamState).write();
};

// Publish message to tangle
const publish = async packet => {
    checkInitialised();

    // Create MAM Payload - STRING OF TRYTES
    const trytes = converter.asciiToTrytes(JSON.stringify(packet));
    const message = mam.create(mamState, trytes);

    // Save new mamState
    saveMAM(message.state);

    // Attach the payload
    await mam.attach(message.payload, message.address, 3, 9, consts.get('APP_NAME'));

    return message.root;
};

// Send a message
const send = async (data, callback = null) => {
    checkInitialised();

    const root = await publish({
        data: data,
        timestamp: (new Date()).toLocaleString()
    });

    if (!storage.has('channels.primary.root').value()) {
        storage.set('channels.primary.root', root).write();
    }

    if (callback) {
        callback(root);
    }

    return root;
};

// Fetch all messages starting at given root
const fetchChannel = async (root = null, callback) => {
    checkInitialised();

    if (!root) {
        if (storage.has('channels.primary.root').value()) {
            root = storage.get('channels.primary.root').value();
        } else {
            callback(null, 'No posts to fetch');
        }
    }

    // Output asyncronously
    mam.fetch(root, MAM_MODE, null, data => {
        callback(JSON.parse(converter.trytesToAscii(data, null)));
    });
};

// Fetch all messages from all channels
const fetchAll = async callback => {
    checkInitialised();

    const channelRoots = [];

    if (storage.has('channels.primary.root').value()) {
        channelRoots.push({
            author: storage.get('profile.username').value() + storage.get('profile.hash').value(),
            root: storage.get('channels.primary.root').value()
        });
    }

    storage.get('following').value().forEach(profile => {
        channelRoots.push({
            author: profile.username + profile.hash,
            root: profile.channels.primary.root
        });
    });

    if (channelRoots.length > 0) {
        channelRoots.forEach(channel => {
            mam.fetch(channel.root, MAM_MODE, null, data => {
                let post = JSON.parse(converter.trytesToAscii(data, null));
                post['author'] = channel.author;
                callback(post);
            });
        });
    } else {
        callback(null, 'Not subscribed to any channels');
    }
};

// Subscribe to a MAM channel
const subscribe = (alias, channelRoot) => {
    checkInitialised();

    // eslint-disable-next-line no-unused-vars
    const res = mam.subscribe(mamState, channelRoot, MAM_MODE);

    // Save new mamState
    // saveMAM(res);

    storage.get('following').find({ username: alias.slice(0, -81), hash: alias.slice(-81) }).set('channels.primary.details', {
        channelKey: null,
        mode: MAM_MODE,
        timeout: 5000,
        root: channelRoot,
        next_root: null,
        active: true
    }).write();

    // mam.listen(channel, resp => {
    //     console.log(resp);
    // });
};

module.exports = storageProvider => {
    storage = storageProvider;

    return {
        init: init,
        send: send,
        fetchChannel: fetchChannel,
        fetchAll: fetchAll,
        subscribe: subscribe
    };
};
