'use strict';

const consts = require('./consts');

module.exports = (appName, fileName, adapterType, readFuncion, writeFunction) => {
    if (!/^[A-Z]{1,10}$/.test(appName)) {
        console.log('ERROR: appName must only contain uppercase letters and be up to 10 characers long.');
        process.exit(1);
    }

    const crypto = require('./cryptography');
    const storage = require('./storage')(fileName, adapterType, readFuncion, writeFunction);
    const profile = require('./profile')(storage);
    const mam = require('./mam')(storage);

    consts.set('APP_NAME', appName);

    const initProfile = async (initMessage, username, userInfo) => {
        if (!(initMessage && username && userInfo)) {
            console.log('Args missing');
        } else if (!/^[a-zA-Z]{1,17}$/.test(username)) {
            console.log('Username must be valid');
        } else {
            storage.set('rsa', crypto.rsaGenerateKeyPair()).write();
            await mam.init(initMessage);
            await profile.uploadProfile(username, userInfo, true);
        }
    };

    const follow = (alias, primaryRoot) => {
        return new Promise((resolve, reject) => {
            profile.follow(alias).then(() => {
                mam.subscribe(alias, primaryRoot);
                resolve();
            }).catch(err => {
                reject(err);
            });
        });
    };

    return {
        initRequired: !profile.profileExists(),
        initProfile: initProfile,
        follow: follow,

        profile: profile,
        feed: mam
    };
};
