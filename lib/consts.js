'use strict';

const IMMUTABLE = [
    'RSA_MOD_LEN',
    'HASH_ALGORITHM',
    'APP_NAME_LENGTH',
    'USERNAME_LENGTH',
    'POW_SUFFIX',
    'PROTOCOL_NAME'
];
const CONSTS = {
    APP_NAME: 'IOTASOCIAL',
    NODE: 'https://nodes.devnet.thetangle.org:443',
    RSA_MOD_LEN: 2048,
    HASH_ALGORITHM: 'sha512',
    APP_NAME_LENGTH: 10,
    USERNAME_LENGTH: 17,
    POW_SUFFIX: '999',
    PROTOCOL_NAME: 'iota-social'
};

const get = search => {
    return CONSTS[search];
};

const set = (key, value) => {
    if (!IMMUTABLE.includes(key)) {
        CONSTS[key.toUpperCase()] = value;
    }
};

module.exports = {
    get: get,
    set: set
};
