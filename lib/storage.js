'use strict';

const low = require('lowdb');
// const crypto = require('./cryptography');

const FILE_EXTENSION = '.iota-social.db.json';

// Initialise local DB
const db = (fileName, adapterType, readFuncion, writeFunction) => {
    if (!fileName) {
        console.error('File name required');
        process.exit(1);
    }

    let fullFileName = fileName + FILE_EXTENSION;
    let adapter;
    const adapterConfig = {
        defaultValue: {
            following: [],
            rsa: {},
            channels: {}
        },
        // TODO encrypt db
        // serialize: (data) => crypto.aesEncrypt(JSON.stringify(data), 'mypassword'),
        // deserialize: (data) => JSON.parse(crypto.aesDecrypt(data, 'mypassword'))
    };

    switch(adapterType) {
        case 'FileSync': {
            const FileSync = require('lowdb/adapters/FileSync');
            adapter = new FileSync(fullFileName, adapterConfig);
            break;
        }
        case 'LocalStorage': {
            const LocalStorage = require('lowdb/adapters/LocalStorage');
            adapter = new LocalStorage(fullFileName, adapterConfig);
            break;
        }
        case 'Custom': {
            if (!(readFuncion && writeFunction)) {
                console.error('Valid read and write functions required');
                process.exit(1);
            }
            const BaseAdapter = require('lowdb/adapters/Base');
            class CustomAdapter extends BaseAdapter {
                read() {
                    return readFuncion();
                }

                write(data) {
                    return writeFunction(data);
                }
            }
            adapter = new CustomAdapter(fullFileName, adapterConfig);
            break;
        }
        default: {
            console.error('Valid adapter type required');
            process.exit(1);
        }
    }

    return low(adapter);
};

module.exports = db;
