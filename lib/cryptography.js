'use strict';

const forge = require('node-forge');
const consts = require('./consts');
/*
const passToKey = passphrase => {
    // TODO generate salt on the fly
    // const salt = crypto.randomBytes(64);
    // const hash = crypto.pbkdf2Sync(passphrase, salt, 232323, 64, consts.get('HASH_ALGORITHM'));
    // const combined = new Buffer(hash.length + salt.length + 8);
    return crypto.pbkdf2Sync(passphrase, 'my_super_secret_salt', 232323, 64, consts.get('HASH_ALGORITHM'));
};

const aesEncrypt = (toEncrypt, passphrase) => {
    const masterkey = passToKey(passphrase);
    // random initialisation vector
    const iv = crypto.randomBytes(16);
    // random salt
    const salt = crypto.randomBytes(64);
    // derive encryption key: 32 byte key length
    const key = crypto.pbkdf2Sync(masterkey, salt, 232323, 32, consts.get('HASH_ALGORITHM'));
    // AES 256 GCM Mode
    const cipher = crypto.createCipheriv('aes-256-gcm', key, iv);
    // encrypt the given input
    const encrypted = Buffer.concat([cipher.update(toEncrypt, 'utf8'), cipher.final()]);
    // extract the auth tag
    const tag = cipher.getAuthTag();
    // generate output
    return Buffer.concat([salt, iv, tag, encrypted]).toString('base64');
};

const aesDecrypt = (toDecrypt, passphrase) => {
    const masterkey = passToKey(passphrase);
    // base64 decoding
    const bData = Buffer.from(toDecrypt, 'base64');
    // convert data to buffers
    const salt = bData.slice(0, 64);
    const iv = bData.slice(64, 80);
    const tag = bData.slice(80, 96);
    const text = bData.slice(96);
    // derive key using; 32 byte key length
    const key = crypto.pbkdf2Sync(masterkey, salt, 232323, 32, consts.get('HASH_ALGORITHM'));
    // AES 256 GCM Mode
    const decipher = crypto.createDecipheriv('aes-256-gcm', key, iv);
    decipher.setAuthTag(tag);
    // encrypt the given text
    const decrypted = decipher.update(text, 'binary', 'utf8') + decipher.final('utf8');
    return decrypted;
};
*/
const rsaEncrypt = (toEncrypt, publicKey) => {
    const buffer = Buffer.from(toEncrypt);
    const encrypted = forge.pki.publicKeyFromPem(publicKey).encrypt(buffer);
    return base64Encode(encrypted);
};

const rsaDecrypt = (toDecrypt, privateKey) => {
    const buffer = base64Decode(toDecrypt, 'utf8');
    const decrypted = forge.pki.privateKeyFromPem(privateKey).decrypt(buffer);
    return decrypted.toString('utf8');
};

const rsaSign = (message, privateKey) => {
    let signer = forge.md[consts.get('HASH_ALGORITHM')].create();
    signer.update(message, 'utf8');
    return base64Encode(forge.pki.privateKeyFromPem(privateKey).sign(signer));
};

const rsaVerify = (message, signature, publicKey) => {
    let verifier = forge.md[consts.get('HASH_ALGORITHM')].create();
    verifier.update(message, 'utf8');
    return forge.pki.publicKeyFromPem(publicKey).verify(verifier.digest().bytes(), base64Decode(signature, 'utf8'));
};

const rsaGenerateKeyPair = () => {
    let keys = forge.pki.rsa.generateKeyPair({ bits: consts.get('RSA_MOD_LEN'), workers: -1 });
    return {
        privateKey: forge.pki.privateKeyToPem(keys.privateKey),
        publicKey: forge.pki.publicKeyToPem(keys.publicKey)
    };
};

const base64Encode = toEncode => {
    return Buffer.from(toEncode).toString('base64');
};

const base64Decode = (toDecode, format = 'ascii') => {
    return Buffer.from(toDecode, 'base64').toString(format);
};

module.exports = {
    // aesEncrypt: aesEncrypt,
    // aesDecrypt: aesDecrypt,
    rsaEncrypt: rsaEncrypt,
    rsaGenerateKeyPair: rsaGenerateKeyPair,
    rsaDecrypt: rsaDecrypt,
    rsaSign: rsaSign,
    rsaVerify: rsaVerify,
    base64Encode: base64Encode,
    base64Decode: base64Decode
};
