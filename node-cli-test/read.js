#!/usr/bin/env node

require('dotenv').config();
const Iota = require('@iota/core');
const Converter = require('@iota/converter');

const seed = process.env.SEED;

const iota = Iota.composeAPI({
    provider: process.env.NODE
});

iota.getNodeInfo().then(node => {
    if (Math.abs(node['latestMilestoneIndex'] - node['latestSolidSubtangleMilestoneIndex']) > 3) {
        console.log('Node is probably not synced!');
    } else {

        iota.getAccountData(seed, { start: 0, security: 2 }).then(account => {
            // console.log(account);
            account.transfers.forEach((transfer, index) => {
                // console.log(transfer)
                let tx = {
                    'hash': transfer[0].hash,
                    'value': transfer[0].value,
                    'confirmed': transfer[0].persistence,
                    'message': transfer[0].signatureMessageFragment
                }
                var message = tx.message.replace(/\d+$/, '');

                console.log(Converter.trytesToAscii(message));
            });
        }).catch(error => {
            console.log(`Request error: ${error.message}`);
        });

    }
}).catch(error => {
    console.log(`Request error: ${error.message}`);
});
